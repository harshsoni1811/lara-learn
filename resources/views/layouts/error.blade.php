@if (count($errors))
@foreach($errors->all() as $error)
<div class="alert alert-danger fade in left-icon">
    <i class="fa-fw fa fa-close"></i>
    {{ $error }}
</div>
@endforeach
@endif