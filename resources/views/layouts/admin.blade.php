<!DOCTYPE html>
<html>
	<head>
		<title>Golabi Admin - Dashboard</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<!-- RTL version css -->
		<!-- <link rel="stylesheet" href="../assets/css/yep-rtl.css"> -->
		
		<!-- Related css to this page -->	
               

		<!-- Yeptemplate css --><!-- Please use *.min.css in production --><!-- Please use *.min.css in production -->
		<!-- favicon -->
	</head>
	<!-- you can add .rtl class for change direction and add lang attribute for change font style ex: lang='ar' -->
	<body id="mainbody" >
		<!-- Available Classes
			* 'container'         	- boxed layout mode (non-responsive: will not work with fixed-sidebar & fixed-ribbon)
			* 'container-fluid'		- Fullwidth layout mode
			* 'fixed-header'		- Fixed header mode
			* 'fixed-ribbon'		- Fixed breadcrumb bar
			* 'fixed-footer'		- Fixed footer
			* 'fixed-sidebar'		- Fixed sidebar mode
			* 'skin-1'				- Blue Skin
			* 'skin-2'				- Black Skin
			* 'skin-3'				- Default Skin
			* 'top-menu'			- Top menu layout menu
			* 'hover-active'		- open submenu in hover mode if you use .top-menu
		-->
		<div id="container" class="container-fluid skin-3" >
			<!-- Add Task in sidebar list modal -->
			<div class="modal fade" id="modal-add-task" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<form class="form-horizontal">
							<div class="modal-header default">
								 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								<h4 class="modal-title" id="myModalLabel1">
									Add Task
								</h4>
							</div>

							<div class="modal-body">
								<!-- Text input-->
								<div class="control-group">
								  	<label class="control-label" for="task-name">Task Name</label>
								  	<div class="controls">
								    	<input id="task-name" name="task-name" type="text" placeholder="" class="form-control">
								  	</div>
								</div>

								<!-- Textarea -->
								<div class="control-group">
								  	<label class="control-label" for="Description">Description</label>
								  	<div class="controls">                     
								    	<textarea id="Description" name="Description" class="form-control"></textarea>
								  	</div>
								</div>

								<!-- Text input-->
								<div class="control-group">
								  	<label class="control-label" for="owner">Owner</label>
								  	<div class="controls">
								    	<input id="owner" name="owner" type="text" placeholder="" class="form-control">
								    
								  	</div>
								</div>
							</div>
							<div class="modal-footer">
								 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> <button type="button" class="btn btn-primary">Save changes</button>
							</div>
						</form>
					</div>										
				</div>									
			</div>
			<!--./ Add Task in sidebar list modal -->
			
			<!-- Add Contact in sidebar list modal -->
			<div class="modal fade" id="modal-add-contact" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header default">
							 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							<h4 class="modal-title" id="myModalLabel2">
								Add Contact
							</h4>
						</div>
						<div class="modal-body">
							<!-- Text input-->
							<div class="control-group">
							  	<label class="control-label" for="name">Name</label>
							  	<div class="controls">
							    	<input id="name" name="name" type="text" placeholder="" class="form-control">
							  	</div>
							</div>

							<!-- Textarea -->
							<div class="control-group">
							  	<label class="control-label" for="Address">Address</label>
							  	<div class="controls">                     
							    	<textarea id="Address" name="Address" class="form-control"></textarea>
							  	</div>
							</div>

							<div class="control-group">
							  	<label class="control-label" for="Phone">Phone</label>
							  	<div class="controls">                     
							    	<input id="Phone" name="Phone" type="number" placeholder="" class="form-control">
							  	</div>
							</div>

							<!-- Text input-->
							<div class="control-group">
							  	<label class="control-label" for="owner">Email</label>
							  	<div class="controls">
							    	<input id="Email" name="Email" type="text" placeholder="" class="form-control">
							    
							  	</div>
							</div>
						</div>
						<div class="modal-footer">
							 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> <button type="button" class="btn btn-primary">Save changes</button>
						</div>
					</div>										
				</div>									
			</div>
			<!--./ Add Contact in sidebar list modal -->
			@include('layouts.header')

			<!-- sidebar menu -->			
			@include('layouts.nav')
			<!-- /end #sidebar -->
			<!-- main content  -->
			<div id="main" class="main">
				@yield('content')
			</div>
			<!-- ./end #main  -->
			
			<!-- footer -->
			@include('layouts.footer')
			<!-- /footer -->
		</div>

		

		<!-- General JS script library-->
                <script src="{!! asset('vendors/jquery/jquery.min.js') !!}"></script>
        


	</body>
</html>