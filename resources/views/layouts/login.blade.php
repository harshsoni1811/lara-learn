<!DOCTYPE html>
<html>
	<head>
		<title>Admin - Login</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
      	<!-- <link rel="stylesheet" href="../assets/css/yep-rtl.css"> -->
		
		<!-- Related css to this page -->	
       
		<!-- Yeptemplate css --><!-- Please use *.min.css in production -->
      
		<!-- favicon -->
		</head>
	
	<!-- You can use .rtl CSS in #login-page -->
	<body id="mainbody" class="login-page" >
		<canvas id="spiders" class="hidden-xs" ></canvas>
		<div class="">
			<div style="margin: 5% auto; position: relative; width: 400px;">		
				@yield('content')
			</div>
		</div>

		
		<!-- General JS script library-->
                <script src="{!! asset('vendors/jquery/jquery.min.js') !!}"></script>

		<!-- Related JavaScript Library to This Pagee -->
		


		<!-- Plugins Script -->
		
	</body>
</html>