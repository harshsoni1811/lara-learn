@extends('layouts.login')
@section('content')
<div id="register-form" class="panel panel-default animated ">

    <div class="panel-body">
        <div class="text-center">
            <h2>Create new account</h2>
        </div>
        @include('layouts.error')
        <div class="row">

            <div class="col-md-12">
                <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}
                    <fieldset>

                        <div class="spacing hidden-md"></div>
                        <div  class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            <input id="login-email-2" type="email" value="{{ old('email') }}" class="form-control" name="email" placeholder="Enter Your Email">
                        </div>	
                        <div class="spacing hidden-md"></div>
                        <div  class="input-group">
                            <span class="input-group-addon"><i class="fa fa-key"></i></span>
                            <input id="login-pass" type="password" class="form-control" name="password" placeholder="Enter Your password">
                        </div>
                        <div class="spacing hidden-md"></div>
                        <div  class="input-group">
                            <span class="input-group-addon"><i class="fa fa-key"></i></span>
                            <input id="login-password-1" type="password" class="form-control" name="password_confirmation" placeholder="Enter Your Confirm Password">
                        </div>
                        <div class="spacing"><br></div>
                        <button type="submit" id="singlebutton2" name="singlebutton" class="btn btn-success btn-sm pull-right">Submit</button>

                    </fieldset>
                </form>
            </div>

        </div>

    </div>
</div>
<div id="q-sign-in" class="panel panel-default">
    <div class="panel-body text-center">
        I have an account? 
        <a href="{{ route('login') }}"> <b>Click here!</b></a>
    </div>
</div>
@endsection
