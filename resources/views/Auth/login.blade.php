@extends('layouts.login')
@section('content')
<div id="sign-form" class="panel panel-default">					
					<div class="panel-body">										 
						<div class="row">
							<div class="col-md-12">
								<div class="text-center">									
									<h2>Admin Login</h2>
									<br>
								</div>
                                                                @include('layouts.error')
								<form class="form-horizontal" method="POST" action="{{ route('login') }}">
                                                                    {{ csrf_field() }}
									<fieldset>
										<div class="spacing hidden-md"></div>
										<div  class="input-group">
	                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
	                                        <input id="login-username" type="text" class="form-control" name="email" value="" placeholder="email">                                        
	                                    </div>
										<div class="spacing"></div>
										<div  class="input-group">
	                                        <span class="input-group-addon"><i class="fa fa-key"></i></span>
	                                        <input id="login-password" type="password" class="form-control" name="password" placeholder="password">
	                                    </div>
										<div class="spacing"></div>
										<div class="checkbox checkbox-primary"> <input id="remember" type="checkbox"  > <label for="remember"> Remember me </label> </div>
                                                                                <button type="submit" id="singlebutton" name="singlebutton" class="btn btn-success btn-sm  pull-right">Sign In</button>
										
									</fieldset>
								</form>
								<a id="forget" href="#" class="grey">Forget Password?</a>
							</div>

						</div>

					</div>
				</div>

				<div id="q-register" class="panel panel-default">
					<div class="panel-body text-center">
						Not a registered user yet? 
						<a href="{{ route('register') }}"> <b>Sign up now!</b></a>
					</div>
				</div>

				<div id="q-sign-in" class="panel panel-default" style="display:none;">
					<div class="panel-body text-center">
						I have an account? 
						<a id="sign-in" href="#"> <b>Click here!</b></a>
					</div>
				</div>

				<div id="forget-form" class="panel panel-default animated " style="display:none;">
					
					<div class="panel-body">
						<div class="text-center">									
							<h2>Golabi Admin Login</h2>
							<h5 class="grey">Reset password your account</h5>
							<br>
						</div>
						<div class="row">							

							<div class="col-md-12">
								<form class="form-horizontal">
									<fieldset>

										<div class="spacing hidden-md"></div>
										<div  class="input-group">
	                                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
	                                        <input id="login-email-1" type="email" class="form-control" name="email" placeholder="Enter Your Email">
	                                    </div>
										<div class="spacing"></div>
										<div  class="input-group">
	                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
	                                        <input id="login-birthday-1" type="text" class="form-control" name="birthday" placeholder="Enter Your Birthday">
	                                    </div>
										<div class="spacing"><br></div>
										<button id="singlebutton1" name="singlebutton" class="btn btn-info btn-sm pull-right">Submit</button>

									</fieldset>
								</form>
							</div>

						</div>

					</div>
				</div>

				
				<p>Copyright 2018 Harsh. All right reserved.</p>
                                @endsection